<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
class CostTableSeeder extends Seeder
{

    public function run()
    {
        DB::table('tbl_costs')->insert([
            'form' => '1',
            'to' => '2',
            'description' => 'test go',
            'cost' => '10000',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
    }
}
