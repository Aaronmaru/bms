<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\User;
use App\Models\Branch;
use App\Models\Bus;
use App\Models\Driver;

class DatabaseSeeder extends Seeder {

    public function run() {

        Model::unguard();

        // factory(Driver::class, 10)->create();
        /*factory(Branch::class, 10)->create();
        factory(Bus::class, 10)->create();
*/
        $this->call(CustomerTableSeeder::class);
        $this->call(CostTableSeeder::class);
        $this->call(UserTableSeeder::class);
        $this->call(DriverTableSeeder::class);
        $this->call(BusTableSeeder::class);
        $this->call(SeatTableSeeder::class);
        $this->call(BranchTableSeeder::class);
        $this->call(PlaceTableSeeder::class);
        $this->call(TimeTableSeeder::class);
        $this->call(BookingTableSeeder::class);
        $this->call(PlaceTimesTableSeeder::class);
        $this->call(DestinationTableSeeder::class);
        $this->call(DepartureTableSeeder::class);

        Model::reguard();
    }
}
