<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class SeatTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('tbl_seats')->insert([
        'status' =>1,
        'created_at' => Carbon::now(),
        'updated_at' => Carbon::now(),
        ]);
    }
}
