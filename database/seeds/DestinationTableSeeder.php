<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class DestinationTableSeeder extends Seeder {

    public function run()
    {
        DB::table('tbl_destinations')->insert([
            'from' => 1,
            'to' => 2,
            'price' => 30000,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        DB::table('tbl_destinations')->insert([
            'from' => 1,
            'to' => 3,
            'price' => 25000,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        DB::table('tbl_destinations')->insert([
            'from' => 1,
            'to' => 4,
            'price' => 20000,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        DB::table('tbl_destinations')->insert([
            'from' => 1,
            'to' => 5,
            'price' => 10000,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        DB::table('tbl_destinations')->insert([
            'from' => 1,
            'to' => 6,
            'price' => 40000,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
    }
}
