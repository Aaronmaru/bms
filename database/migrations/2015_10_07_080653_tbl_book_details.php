<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TblBookDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_book_details', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('booking_id')->unsigned();

            $table->integer('bus_id')->unsigned();
            $table->integer('seat_id')->unsigned();

            $table->integer('departure_id')->unsigned();
            $table->integer('destination_id')->unsigned();

           /* $table->foreign('booking_id')->references('id')->on('tbl_booking')->onDelete('cascade');
            $table->foreign('bus_id')->references('id')->on('tbl_buses')->onDelete('cascade');
            $table->foreign('seat_id')->references('id')->on('tbl_seats')->onDelete('cascade');*/
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tbl_book_details');
    }
}
