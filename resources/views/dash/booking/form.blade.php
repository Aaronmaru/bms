<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">{!! $pageHeader !!}</h1>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
            <div class="panel-body">
            @include('dash.common.errors')
                <div class="row">
                    <div class="col-lg-8">
                            {{--<div class="form-group">
                                {!! Form::label('place_from', 'Place From')!!}
                                {!! Form::select('place_from', $place_from, null, ['id'=>'place_from','class'=>'form-control'])!!}
                            </div>
                            <div class="form-group">
                                {!! Form::label('place_to', 'Place To')!!}
                                {!! Form::select('place_to', $place_to, null, ['id'=>'place_to','class'=>'form-control'])!!}
                            </div>--}}
                            <div class="form-group">
                                <label class="checkbox-inline"><input type="radio" name="trip_type" value="0">One Way</label>
                                <label class="checkbox-inline"><input type="radio" name="trip_type" value="1">Round Trip</label>
                            </div>
                            <div class="form-group">
                               {!! Form::label('from','From')!!}
                               {!! Form::select('from',$froms,null,['id'=>'selFrom','class'=>'form-control'])!!}
                            </div>
                            <div class="form-group">
                               {!! Form::label('to','To')!!}
                               {!! Form::select('to',$tos,null,['id'=>'selTo','class'=>'form-control'])!!}
                            </div>
                            <div class="form-group">
                               {!! Form::label('price','Price')!!}
                               <p class="form-control">{{ $price }}</p>
                            </div>
                            <div class="form-group">
                               {!! Form::label('start','Time Start')!!}
                               {!! Form::select('start',$starts,null,['id'=>'start','class'=>'form-control'])!!}
                            </div>
                            <div class="form-group">
                               {!! Form::label('stop','Time Stop')!!}
                               {!! Form::select('stop',$stops,null,['id'=>'stop','class'=>'form-control'])!!}
                            </div>
                            <div class="form-group">
                               {!! Form::label('bus_id','Bus')!!}
                               {!! Form::select('bus_id',$buses,null,['id'=>'bus_id','class'=>'form-control'])!!}
                            </div>
                            <div class="form-group">
                                {!! Form::label('seat_amount','Seat Amount')!!}
                                {!! Form::select('seat_amount',$seat_amounts,null,['id'=>'seat_amount','class'=>'form-control'])!!}
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    @foreach ($seats as $seat)
                                        <div class="col-md-1">
                                            <label class="checkbox-inline"><input type="checkbox" name="seat_id" value="{{ $seat }}">{{ $seat }}</label>
                                        </div>
                                    @endforeach
                               </div>
                            </div>


                            {{--<div class="form-group">
                                {!! Form::label('time_id','From')!!}
                                {!! Form::select('time_id',$time_from,null,['id'=>'time_id','class'=>'form-control'])!!}
                            </div>
                            <div class="form-group">
                                {!! Form::label('time_id','To')!!}
                                {!! Form::select('time_id',$time_to,null,['id'=>'time_id','class'=>'form-control'])!!}
                            </div>--}}

                           {{-- <div class="form-group">
                                {!! Form::label('seat_price','Seat Price')!!}
                                {!! Form::select('seat_price',[1,2,3],null,['id'=>'seat_price','class'=>'form-control'])!!}
                            </div>--}}
                            <div class="form-group">
                                {!! Form::submit($submitButtonText,['class'=>'btn btn-primary'])!!}
                            </div>
                    </div>
                </div>
            </div>
    </div>
</div>
<hr/><br/>