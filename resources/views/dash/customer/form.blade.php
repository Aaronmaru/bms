<div class="form-group">
    {!! Form::label('firstname','First Name') !!}
    {!! Form::text('firstname', null, ['class' => 'form-control']) !!}
</div>
<div class="form-group">
    {!! Form::label('lastname','Last Name') !!}
    {!! Form::text('lastname', null, ['class' => 'form-control']) !!}
</div>
<div class="form-group">
    {!! Form::label('username','Username') !!}
    {!! Form::text('username', null, ['class' => 'form-control']) !!}
</div>
<div class="form-group">

    {!! Form::label('email','Examplemail') !!}
    {!! Form::email('email', null, ['class' => 'form-control']) !!}
</div>
<div class="form-group">

    {!! Form::label('phone','Phone') !!}
    {!! Form::password('phone', ['class' => 'form-control']) !!}
</div>
<div class="form-group">

    {!! Form::label('password','Password') !!}
    {!! Form::password('password', ['class' => 'form-control']) !!}
</div>
<div class="form-group">
    {!! Form::label('password_confirmation','Confirm Password') !!}
    {!! Form::password('password_confirmation', ['class' => 'form-control']) !!}
</div>

{!! Form::submit($submittype, ['class' => 'btn btn-primary form-control']) !!}
