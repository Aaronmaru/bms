@extends('dash')

@section('content')
	<style>

		.header_right {
			float: right;
		}

	</style>
    <h1>Time</h1>
       <div class="panel">
       		<div class="header">
       			<div class="checkbox">
       				<span class="header_left">
	       				<label>
	       					<input type="checkbox" class="cbxAll" /> Check All
	       				</label>
       				</span>
       				<span class="header_right">
       					<button type="button" class="btn btn-success btnAddNewTime">+ Add New Time</button>
       					<button type="button" class="btn btn-danger">- Delete All</button>
       				</span>
       			</div>
       		</div>
       		<br />
       		<div class="body">
	       		<table class="table table-bordered" id="tblTime">
	       			<tr>
	       				<th></th>
		       			<th>ID</th>
		       			<th>Time</th>
		       			<th>Action</th>
	       			</tr>

	    			@foreach ($times as $time)
	       			<tr data-id="{{ $time['id'] }}">
	       				<td>
	       					<input type="checkbox" class="cbxTime" />
	       					
	       				</td>
	       				<td>{{ $time['id'] }}</td>
	       				<td>{{ $time['time'] }}</td>
	       				<td>
							{!! Form::model($time, ['route' => ['dash.time.destroy', $time['id']], 'method' => 'DELETE' ]) !!}
		       					<button type="button" class="btn btn-success btnUpdateTime"><span class="glyphicon glyphicon-pencil"></span> Update</button>
								
								<button type="submit" class="btn btn-danger btnDeleteTime" method='DELETE'><span class="glyphicon glyphicon-trash"></span> Delete</button>
							{!! Form::close() !!}
	       				</td>
	       			</tr>
	    			@endforeach
	       		</table>
	       	</div>
	       	<div class="footer">
	       		<ul class="pagination">
	       			<li>
		       			<a href="#" arial-label="Previous">
		       				<span arial-hidden="true">&laquo;</span>
		       			</a>
	       			</li>
	       			<li><a href="#">1</a></li>
	       			<li><a href="#">2</a></li>
	       			<li><a href="#">3</a></li>
	       			<li><a href="#">4</a></li>
	       			<li><a href="#">5</a></li>
	       			<li><a href="#">6</a></li>
	       			<li><a href="#">7</a></li>
	       			<li>
		       			<a href="#" arial-label="Next">
		       				<span arial-hidden="true">&raquo;</span>
		       			</a>
	       			</li>
	       		</ul>
	       	</div>
       </div>

@endsection
