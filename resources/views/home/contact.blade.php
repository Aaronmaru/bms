@extends('home')

@section('content')

    <div class="contact">
        <div class="container">
            <div class="contact-head">
                <h2>CONTACT</h2>
                <form>
                    <div class="col-md-6 contact-left">
                        <input type="text" placeholder="Name" required/>
                        <input type="text" placeholder="E-mail" required/>
                        <input type="text" placeholder="Phone" required/>
                    </div>
                    <div class="col-md-6 contact-right">
                        <textarea placeholder="Message"></textarea>
                        <input type="submit" value="SEND" />
                    </div>
                    <div class="clearfix"></div>
                </form>
            </div>
            <div class="address">
                <h3>Our Locations</h3>
                <div class="locations">
                    <ul>
                        <li><span></span></li>
                        <li>
                            <div class="address-info">
                                <h4>HEAD OFFICE PHNOM PENH</h4>
                                <p>Sangkat Phsar Thmey II, Khan Daun penh,</p>
                                <p>Phnom Penh, Cambodia,</p>
                                <p>Telephone   : (855-12) 012 631 545 / (855-23) 210 359</p>
                                <p>Mail   :   168@ppsoryatransport.com</p>
                                <p>Fax :   (855-23) 992 569</p>
                                <h5><a href="https://www.google.com/maps/d/edit?mid=zcCMIwFqlTfM.kkQKuS8L3fKs&usp=sharing">Visit on Google Maps >></a></h5>
                            </div>
                        </li>
                    </ul>
                    <ul>
                        <li><span></span></li>
                        <li>
                            <div class="address-info">
                                <h4>SIEM REAP OFFICE</h4>
                                <p>#588, Tep Vong Street, Mondul I,Khum Svay Dang Kum,</p>
                                <p>Srok Siem Reap, Siem Reap, Cambodia,</p>
                                <p>Telephone   : (855-92) 181 800 / (855-12) 235 618</p>
                                <p>Mail   :   168@ppsoryatransport.com</p>
                                <p>Fax :   (855-23) 992 569</p>
                                <h5><a href="https://www.google.com.kh/maps/place/Samdech+Tep+Vong+St,+Krong+Siem+Reap/@13.3584881,103.8520578,19.25z/data=!4m2!3m1!1s0x3110176e51dea165:0xeecc8ec85f9d08cd?hl=en">Visit on Google Maps >></a></h5>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="contact-map">
                <iframe src="https://mapsengine.google.com/map/u/0/embed?mid=zcCMIwFqlTfM.kkQKuS8L3fKs" ></iframe>
                {{-- <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1578265.0941403757!2d-98.9828708842255!3d39.41170802696131!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x54eab584e432360b%3A0x1c3bb99243deb742!2sUnited+States!5e0!3m2!1sen!2sin!4v1407515822047"> </iframe> --}}
            </div>
        </div>
    </div>

@stop

@section('bottom_content')

@stop
