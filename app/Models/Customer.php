<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model {
    public $table = 'tbl_customers';
    
    protected $fillable = [
        'firstname',
        'lastname',
        'username',
        'email',
        'password'
    ];
}
