<?php

namespace App\Http\Controllers\Dash;

//use Illuminate\Http\Request;
// use App\Http\Requests;
use App\Http\Requests\BranchRequest;
use App\Http\Controllers\Controller;

use App\Models\User;
use App\Models\Branch;


class BranchController extends Controller {

    public function index() {
        $data['title'] = 'Branch';
        $data['branches'] = Branch::all();
        return view('dash.branch.index', $data);
    }

    public function json() {
        return Branch::all()->toArray();



        $items = Branch::all()->toJson();
        return Datatables::of($items)
            ->addColumn('action', function($item){
                return "<a href='#edit-$item->id' class='btn btn-xs btn-primary'><i class='glyphicon glyphicon-edit'></i> Edit</a>";
            })
            ->editColumn('id', 'ID: {{$id}}')
            ->make(true)
            ;
    }

    public function create() {
        $data['users'] = User::lists('username', 'id');
        return view('dash.branch.create', $data);
    }

    public function store(BranchRequest $request) {
        dd($request);
    }

    public function show($id) {
        $data['branch'] = Branch::find($id);
        return view('dash.branch.show', $data);
    }

    public function edit($id) {
        $data['branch'] = Branch::find($id);
        $data['users'] = User::lists('username', 'id');
        return view('dash.branch.edit', $data);
    }

    public function update(BranchRequest $request, $id) {
        //
    }

    public function destroy($id) {
        //
    }
}
