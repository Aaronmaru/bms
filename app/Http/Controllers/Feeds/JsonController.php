<?php

namespace App\Http\Controllers\Feeds;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\User;
use App\Models\Time;

class JsonController extends Controller {

    public function users() {
        return User::all()->toArray();
    }

    public function times() {
        return Time::all()->toArray();
    }

}
