<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Branch;
use App\Models\Place;

class HomeController extends Controller {

    public function __construct() {

    }

    public function index() {
        $data['title'] = 'Home Controller';
        return view('home.index', $data);
    }

    public function branches() {
        $data['title'] = 'Branches';
        $data['branches'] = Branch::all();
        // $data['branchs'] = Branch::all()->toArray();
        // dd($data);
        return view('home.branches', $data);
    }

    public function services() {

        $data['title'] = 'Services';

        //$data['services'] = Services::all()->toArray();



        return view('home.services', $data);
    }

    public function promotion() {
        $data['title'] = 'Promotion';
        // $data['promotion'] = Promotion::all()->toArray();
        return view('home.promotion', $data);
    }

    public function about() {
        $data['title'] = 'About';
        return view('home.about', $data);
    }

    public function contact() {
        $data['title'] = 'Contact';
        return view('home.contact', $data);
    }




}
