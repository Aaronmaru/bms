<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class BranchRequest extends Request {

    public function authorize() {
        return true;
    }

    public function rules() {
        return [
            'name' => 'required|min:6|max:255',
            'address' => 'required',
            'phone' => 'required|min:9'
        ];
    }
}
